package ee.rescue.tap.service;

import ee.rescue.tap.model.WeekEST;
import org.springframework.stereotype.Service;

@Service
public class TableService {
    public String translateWeekDay(int weekdayValue) {
        switch (weekdayValue) {
            case 1: return WeekEST.ESMASPÄEV.toString().toLowerCase();
            case 2: return WeekEST.TEISIPÄEV.toString().toLowerCase();
            case 3: return WeekEST.KOLMAPÄEV.toString().toLowerCase();
            case 4: return WeekEST.NELJAPÄEV.toString().toLowerCase();
            case 5: return WeekEST.REEDE.toString().toLowerCase();
            case 6: return WeekEST.LAUPÄEV.toString().toLowerCase();
            case 7: return WeekEST.PÜHAPÄEV.toString().toLowerCase();
        }
        return "MyError";
    }
}
