package ee.rescue.tap.service;

import ee.rescue.tap.model.User;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class UserDataService {

    private static Object getCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();

            case BOOLEAN:
                return cell.getBooleanCellValue();

            case NUMERIC:
                return cell.getNumericCellValue();
        }
        return null;
    }

    // https://www.codejava.net/coding/how-to-read-excel-files-in-java-using-apache-poi
    public static List<User> readUserInfoFromExcelFile(String path) throws IOException {
        List<User> usersList = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(new File(path));

        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();

        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (nextRow.getRowNum() == 0) {
                continue;
            }
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            User aUser = new User();
            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();
                switch (columnIndex) {
                    case 0:
                        aUser.setBrigadeID(((Double) (getCellValue(nextCell))).intValue()); // NullPointerExecptioni võimalus?
                        break;
                    case 1:
                        aUser.setSquadNumber(((Double) getCellValue(nextCell)).intValue());
                        break;
                    case 2:
                        aUser.setFirstName((getCellValue(nextCell)).toString());
                        break;
                    case 3:
                        aUser.setLastName((getCellValue(nextCell)).toString());
                        break;
                    case 4:
                        aUser.setIdCode((getCellValue(nextCell)).toString());
                        break;
                    case 5:
                        aUser.setPhone((getCellValue(nextCell)).toString());
                        break;
                    case 6:
                        aUser.setEmail((getCellValue(nextCell)).toString());
                        break;
                    case 7:
                        aUser.setAddress((getCellValue(nextCell)).toString());
                        break;
                }
            }
            usersList.add(aUser);
        }

        workbook.close();
        inputStream.close();

        return usersList;
    }
}
