package ee.rescue.tap.repository;

import ee.rescue.tap.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;


import java.util.List;


@Repository
public class TapRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<User> fetchUsers() {
        List<User> users = jdbcTemplate.query("SELECT u.*, b.name AS brigadeName, r.name AS roleName FROM user u " +
                        "INNER JOIN brigade b ON u.brigadeID = b.id INNER JOIN userrole ur ON u.id = ur.user_id INNER JOIN role r ON ur.role_id = r.id",
                mapUserRowsWithRole);

//        for (int i = 0; i < users.size(); i++) {
//            List<String> userCompetences = jdbcTemplate.queryForList("SELECT symbol AS competenceSymbols FROM competence c " +
//                            "INNER JOIN usercompetence uc ON c.id = uc.competence_id WHERE uc.user_id = ?",
//                    new Object[]{users.get(i).getId()}, String.class);
//            users.get(i).setUserCompetences(userCompetences);
//        }

        return users;
    }

    public User fetchUser(int id) {
        List<User> users = jdbcTemplate.query("SELECT u.*, b.name AS brigadeName, r.name AS roleName FROM user u INNER JOIN brigade b ON u.brigadeID = b.id " +
                "INNER JOIN userrole ur ON u.id = ur.user_id INNER JOIN role r ON ur.role_id = r.id WHERE u.id = ?", new Object[]{id}, mapUserRowsWithRole);

//        List<String> userCompetences = jdbcTemplate.queryForList("SELECT symbol AS competenceSymbols FROM competence c " +
//                        "INNER JOIN usercompetence uc ON c.id = uc.competence_id WHERE uc.user_id = ?",
//                new Object[]{id}, String.class);
//
//        users.get(0).setUserCompetences(userCompetences);

        return users.size() > 0 ? users.get(0) : null;
    }

    public void deleteUser(int id) {
        jdbcTemplate.update("DELETE ur.*, uc.* FROM userrole ur INNER JOIN user u ON u.id = ur.user_id INNER JOIN usercompetence uc ON u.id = uc.user_id WHERE u.id = ?", id);
        jdbcTemplate.update("DELETE FROM user WHERE id = ?", id);
    }

    public void addUser(User user) {
        int brigadeID = user.getBrigadeID();
        int squadNumber = user.getSquadNumber();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String idCode = user.getIdCode();
        String phone = user.getPhone();
        String email = user.getEmail();
        String address = user.getAddress();
        String roleName = user.getRoleName();
//        List<String> userCompetences = user.getUserCompetences();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(
                                    "INSERT INTO user (brigadeID, squadNumber, firstName, lastName, idCode, phone, email, address) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                                    Statement.RETURN_GENERATED_KEYS
                            );
                    ps.setInt(1, brigadeID);
                    ps.setInt(2, squadNumber);
                    ps.setString(3, firstName);
                    ps.setString(4, lastName);
                    ps.setString(5, idCode);
                    ps.setString(6, phone);
                    ps.setString(7, email);
                    ps.setString(8, address);
                    return ps;
                },
                keyHolder
        );

        int roleId = jdbcTemplate.queryForObject("SELECT id FROM role WHERE name = ?", Integer.class, roleName);
        jdbcTemplate.update("INSERT INTO userrole (user_id, role_id) VALUES (?, ?)", keyHolder.getKey().intValue(), roleId);

//        for (int i = 0; i < userCompetences.size(); i++) {
//            int competenceId = jdbcTemplate.queryForObject("SELECT id FROM competence WHERE name = ?", Integer.class, userCompetences.get(i));
//            jdbcTemplate.update("INSERT INTO usercompetence (user_id, competence_id) VALUES (?, ?)", keyHolder.getKey().intValue(), competenceId);
//        }
    }

    public void updateUser(User user) { // parandada!!! (kasutaja sisestab pädevuste sümbolid)
        jdbcTemplate.update("UPDATE user SET brigadeID = ?, squadNumber = ?, firstName = ?, lastName = ?, idCode = ?, phone = ?, email = ?, address = ? WHERE id = ?",
                user.getBrigadeID(), user.getSquadNumber(), user.getFirstName(), user.getLastName(), user.getIdCode(), user.getPhone(), user.getEmail(), user.getAddress(), user.getId());

        int roleId = jdbcTemplate.queryForObject("SELECT id FROM role WHERE name = ?", Integer.class, user.getRoleName());
        jdbcTemplate.update("UPDATE userrole SET role_id = ? WHERE user_id = ?", roleId, user.getId());

//        jdbcTemplate.update("UPDATE usercompetence uc SET competence_id = ? SELECT c.symbol FROM competence c INNER JOIN uc ON uc.competence_id = c.id WHERE user_id = ?",
//                user.getUserCompetences(), user.getId());

        // Hetkel tuleb kõik väljad ära täita, kui tahta eraldi, siis tuleb OPTIONAL sql kasutada vms.
        // Nt kui veebis input lahter on tühjaks jäetud, siis ei ole väärtus 'tühi' vaid väärtust ei muudeta.
    }

    public void addUsersFromFile(List<User> list) {
        System.out.println("Andmebaasi lisatud uued töötajad: ");
        for (int i = 0; i < list.size(); i++) {
            addUser(list.get(i));
            System.out.println((i + 1) + ": " + list.get(i).getFirstName() + " " + list.get(i).getLastName());
        }
    }

    private RowMapper<User> mapUserRowsWithRole = (rs, rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setBrigadeID(rs.getInt("brigadeID"));
        user.setSquadNumber(rs.getInt("squadNumber"));
        user.setFirstName(rs.getString("firstName"));
        user.setLastName(rs.getString("lastName"));
        user.setIdCode(rs.getString("idCode"));
        user.setPhone(rs.getString("phone"));
        user.setEmail(rs.getString("email"));
        user.setAddress(rs.getString("address"));
        user.setBrigadeName(rs.getString("brigadeName"));
        user.setRoleName(rs.getString("roleName"));
        return user;
    };

    private RowMapper<User> mapUserRows = (rs, rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setBrigadeID(rs.getInt("brigadeID"));
        user.setSquadNumber(rs.getInt("squadNumber"));
        user.setFirstName(rs.getString("firstName"));
        user.setLastName(rs.getString("lastName"));
        user.setIdCode(rs.getString("idCode"));
        user.setPhone(rs.getString("phone"));
        user.setEmail(rs.getString("email"));
        user.setAddress(rs.getString("address"));
        return user;
    };
}
