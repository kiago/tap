package ee.rescue.tap.repository;

import com.google.gson.Gson;
import ee.rescue.tap.model.Day;
import ee.rescue.tap.model.Holiday;
import ee.rescue.tap.model.WorkMonth;
import ee.rescue.tap.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Repository
@CrossOrigin("*")
public class TableRepository {

    private final String YEAR_STRING = "2020";
    private final boolean LEAP_YEAR = true;

    private List<Holiday> publicHolidays;
    private List<ee.rescue.tap.model.Month> listOfMonth;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TableService tableService;

    @Bean
    private RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @PostConstruct
    private void loadPublicHolidays() {
        publicHolidays = getHolidaysList();
        listOfMonth = getListOfMonths();
    }

    private List<Holiday> getHolidaysList() {
        String result = restTemplate().exchange("https://xn--riigiphad-v9a.ee/?output=json", HttpMethod.GET, null, String.class).getBody();
        Gson gson = new Gson();
        return Arrays.asList(gson.fromJson(result, Holiday[].class));
    }

    public List<Holiday> getPublicHolidays() {
        return publicHolidays;
    }

    public ee.rescue.tap.model.Month getMonthDays(int monthIntValue) {
        return getMonthFromList(monthIntValue);
    }

    public List<ee.rescue.tap.model.Month> getListOfMonths() {
        List<ee.rescue.tap.model.Month> listOfMonths = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            // Create month
            ee.rescue.tap.model.Month newMonth = new ee.rescue.tap.model.Month();
            newMonth.setNumValue(i);
            newMonth.setName(java.time.Month.of(i).getDisplayName(TextStyle.FULL, Locale.getDefault()));
            newMonth.setSymbol(java.time.Month.of(i).getDisplayName(TextStyle.SHORT, Locale.getDefault()));
            int daysAmount = java.time.Month.of(i).length(LEAP_YEAR);

            for (int day = 1; day <= daysAmount; day++) {
                // Create days into month
                String monthString = i < 10 ? "0" + i : "" + i;
                String dateString = day < 10 ? "0" + day : "" + day;
                String fullDate = YEAR_STRING + "-" + monthString + "-" + dateString;

                DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate date = LocalDate.parse(fullDate, DTF);

                int weekdayValue = date.getDayOfWeek().getValue();
                String weekday = tableService.translateWeekDay(weekdayValue);

                Day newDay = new Day(fullDate, weekday, weekdayValue,0, weekday); // weekday as default holidayTitle

                for (Holiday holiday: publicHolidays) {
                    if (fullDate.equals(holiday.getDate())) {
                        newDay.sethHolidayId(holiday.getKind_id());
                        newDay.sethTitle(holiday.getTitle());
                    } else if (date.getDayOfWeek().getValue() == 6 || date.getDayOfWeek().getValue() == 7) {
                        newDay.sethHolidayId(5);
                    }
                }

                newMonth.addDays(newDay);
            }
            listOfMonths.add(newMonth);
        }
        return listOfMonths;
    }

    private ee.rescue.tap.model.Month getMonthFromList(int i) { return listOfMonth.get(i-1); }

    public List<WorkMonth> getWorkingDays(int monthInt) { // selekteerimine peaks olema kuu põhine, siis on võimalik töötunnid backendis kokku arvutada
//        List<WorkMonth> months = jdbcTemplate.query("SELECT u.firstName AS firstName, u.lastName AS lastName, r.symbol AS roleSymbol, w.* FROM workdays w INNER JOIN user u ON w.id = u.id INNER JOIN userrole ur ON u.id = ur.user_id INNER JOIN role r ON ur.role_id = r.id", mapTableRow);
        List<WorkMonth> months = jdbcTemplate.query("SELECT * FROM (SELECT u.firstName AS firstName, u.lastName AS lastName, r.symbol AS roleSymbol, w.* " +
                "FROM workdays w INNER JOIN user u ON w.id = u.id INNER JOIN userrole ur ON u.id = ur.user_id INNER JOIN role r ON ur.role_id = r.id) AS yearTable " +
                "WHERE yearTable.m = ?", new Object[]{monthInt}, mapTableRow);
        return months;
}
    private RowMapper<WorkMonth> mapTableRow = (rs, rowNum) -> {
        WorkMonth workMonth = new WorkMonth();
        workMonth.setFirstName(rs.getString("firstName"));
        workMonth.setLastName(rs.getString("lastName"));
        workMonth.setRoleSymbol(rs.getString("roleSymbol"));
        workMonth.setId(rs.getInt("id"));
        workMonth.setD01(rs.getString("1"));
        workMonth.setD02(rs.getString("2"));
        workMonth.setD03(rs.getString("3"));
        workMonth.setD04(rs.getString("4"));
        workMonth.setD05(rs.getString("5"));
        workMonth.setD06(rs.getString("6"));
        workMonth.setD07(rs.getString("7"));
        workMonth.setD08(rs.getString("8"));
        workMonth.setD09(rs.getString("9"));
        workMonth.setD10(rs.getString("10"));
        workMonth.setD11(rs.getString("11"));
        workMonth.setD12(rs.getString("12"));
        workMonth.setD13(rs.getString("13"));
        workMonth.setD14(rs.getString("14"));
        workMonth.setD15(rs.getString("15"));
        workMonth.setD16(rs.getString("16"));
        workMonth.setD17(rs.getString("17"));
        workMonth.setD18(rs.getString("18"));
        workMonth.setD19(rs.getString("19"));
        workMonth.setD20(rs.getString("20"));
        workMonth.setD21(rs.getString("21"));
        workMonth.setD22(rs.getString("22"));
        workMonth.setD23(rs.getString("23"));
        workMonth.setD24(rs.getString("24"));
        workMonth.setD25(rs.getString("25"));
        workMonth.setD26(rs.getString("26"));
        workMonth.setD27(rs.getString("27"));
        workMonth.setD28(rs.getString("28"));
        workMonth.setD29(rs.getString("29"));
        workMonth.setD30(rs.getString("30"));
        workMonth.setD31(rs.getString("31"));
        return workMonth;
    };
}
