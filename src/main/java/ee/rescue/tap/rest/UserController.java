package ee.rescue.tap.rest;

import ee.rescue.tap.dto.GenericResponseDto;
import ee.rescue.tap.dto.JwtRequestDto;
import ee.rescue.tap.dto.JwtResponseDto;
import ee.rescue.tap.dto.UserRegistrationDto;
import ee.rescue.tap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}
