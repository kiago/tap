package ee.rescue.tap.rest;

import ee.rescue.tap.model.UploadResponse;
import ee.rescue.tap.repository.TapRepository;
import ee.rescue.tap.service.FileService;
import ee.rescue.tap.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Path;

@RestController
@RequestMapping("/files")
@CrossOrigin("*")
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private TapRepository tapRepository;

    @PostMapping("/upload")
    public UploadResponse uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        String fileName = fileService.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("files/file/" + fileName).toUriString();
        return new UploadResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
    }

    @GetMapping("/file/{fileName}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) throws IOException {
        Resource resource = fileService.loadFileAsResource(fileName);
        String contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    // Batch import body parameter e.g '/import?path=C:\dev\tap-data\example_import_table.xlsx'
    @PostMapping("/import")
    public void addUsersFromList(@RequestParam String path) throws IOException {
        tapRepository.addUsersFromFile(userDataService.readUserInfoFromExcelFile(path));
    }

    // return file fullpath ilma üleslaadimiseta
    @PostMapping("/filepath")
    public String importFilePath(@RequestParam("file") MultipartFile file) throws IOException {
        String fileName = fileService.storeFile(file);
        Path targetLocation = this.fileService.getUploadDir().resolve(fileName);
        String filepath = targetLocation.toString();
        return filepath;
    }
}
