package ee.rescue.tap.rest;

import ee.rescue.tap.model.WorkMonth;
import ee.rescue.tap.repository.TableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/table")
@CrossOrigin("*")
public class DayController {

    @Autowired
    private TableRepository tableRepository;

    @GetMapping("/month")
    public ee.rescue.tap.model.Month getMonthWithDays(@RequestParam("m") int monthInt) {
        return tableRepository.getMonthDays(monthInt);
    }

    @GetMapping("/workdays")
    public List<WorkMonth> getWorkDays(@RequestParam("m") int monthInt) {
        return tableRepository.getWorkingDays(monthInt);
    }
}