package ee.rescue.tap.controller;

import ee.rescue.tap.model.User;
import ee.rescue.tap.repository.TapRepository;
import ee.rescue.tap.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/")
@CrossOrigin("*")
public class TapController {

    @Autowired
    public TapRepository tapRepository;

    @Autowired
    public UserDataService userDataService;

    @GetMapping("/users")
    public List<User> getUsers() {
        return tapRepository.fetchUsers();
    }

    @GetMapping("/user")
    public User getUserById(@RequestParam("id") int id) {
        return tapRepository.fetchUser(id);
    }

    @DeleteMapping("/user")
    public void deleteUserById(@RequestParam ("id") int id) {
        String deletedUserName = tapRepository.fetchUser(id).getFirstName() + " " + tapRepository.fetchUser(id).getLastName();
        tapRepository.deleteUser(id);
        System.out.println(String.format("REMOVE: User <%d: %s> account removed", id, deletedUserName));
    }

    @PostMapping("/user")
    public void addUser(@RequestBody User user) {
        tapRepository.addUser(user);
        System.out.println(String.format("ADD: New user <%s %s> account added", user.getFirstName(), user.getLastName()));
    }

    @PutMapping("/user")
    public void updateUser(@RequestBody User user) {
        tapRepository.updateUser(user);
        System.out.println(String.format("UPDATE: User <%d: %s %s> attributes updated", user.getId(), user.getFirstName(), user.getLastName()));
    }
}
