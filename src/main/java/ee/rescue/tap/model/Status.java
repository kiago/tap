package ee.rescue.tap.model;

public enum Status {
    WORKING, VACATION, FREE
}
