package ee.rescue.tap.model;

public class Brigade {
    private int id;
    private int centerID;
    private String name;
    private String phone;
    private String address;

    public Brigade() { }

    public Brigade(int id, int centerID, String name, String phone, String address) {
        this.id = id;
        this.centerID = centerID;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public int getCenterID() { return centerID; }
    public void setCenterID(int centerId) { this.centerID = centerID; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }
    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }
}
