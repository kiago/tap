package ee.rescue.tap.model;

public class Holiday {
    private String date;
    private String title;
    private String notes;
    private String kind;
    private int kind_id;

    public Holiday() { }

    public Holiday(String date, String title, String notes, String kind, int kind_id) {
        this.date = date;
        this.title = title;
        this.notes = notes;
        this.kind = kind;
        this.kind_id = kind_id;
    }
    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public String getNotes() { return notes; }
    public void setNotes(String notes) { this.notes = notes; }
    public String getKind() { return kind; }
    public void setKind(String kind) { this.kind = kind; }
    public int getKind_id() { return kind_id; }
    public void setKind_id(int kind_id) { this.kind_id = kind_id; }
}
