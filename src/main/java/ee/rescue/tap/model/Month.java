package ee.rescue.tap.model;

import java.util.ArrayList;
import java.util.List;

public class Month {
    private String name;
    private String symbol;
    private int numValue;
    private List<Day> days = new ArrayList<>();
    private List<Holiday> holidays = new ArrayList<>();

    public Month() { }

    public Month(String name, String symbol, int numValue, List<Day> days, List<Holiday> holidays) {
        this.name = name;
        this.symbol = symbol;
        this.numValue = numValue;
        this.days = days;
        this.holidays = holidays;
    }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getSymbol() { return symbol; }
    public void setSymbol(String symbol) { this.symbol = symbol; }
    public int getNumValue() { return numValue; }
    public void setNumValue(int numValue) { this.numValue = numValue; }
    public List<Day> getDays() { return days; }

    public void addDays(Day day) {
        this.days.add(day);
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }
    public List<Holiday> getHolidays() {
        return holidays;
    }

    public void addHolidays(Holiday holiday) {
        this.holidays.add(holiday);
    }

    public void setHolidays(List<Holiday> holidays) {
        this.holidays = holidays;
    }
}
