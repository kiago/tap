package ee.rescue.tap.model;

import java.util.List;

public class User {
    private int id;
    private int brigadeID;
    private int squadNumber;
    private String firstName;
    private String lastName;
    private String idCode;
    private String phone;
    private String email;
    private String address;
    private String username;
    private String password;
    private String brigadeName;
    private String roleName;
//    private List<String> userCompetences;

    public User() { }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
    public User(int id, int brigadeID, int squadNumber, String firstName, String lastName, String idCode, String phone, String email, String address, String brigadeName, String roleName, List<String> userCompetences) {
        this.id = id;
        this.brigadeID = brigadeID;
        this.squadNumber = squadNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCode = idCode;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.brigadeName = brigadeName;
        this.roleName = roleName;
//        this.userCompetences = userCompetences;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public int getBrigadeID() { return brigadeID; }
    public void setBrigadeID(int brigadeID) { this.brigadeID = brigadeID; }
    public int getSquadNumber() { return squadNumber; }
    public void setSquadNumber(int squadNumber) { this.squadNumber = squadNumber; }
    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }
    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }
    public String getIdCode() { return idCode; }
    public void setIdCode(String idCode) { this.idCode = idCode; }
    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }
    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }
    public String getBrigadeName() { return brigadeName; }
    public void setBrigadeName(String brigadeName) { this.brigadeName = brigadeName; }
    public String getRoleName() { return roleName; }
    public void setRoleName(String roleName) { this.roleName = roleName; }
//    public List<String> getUserCompetences() { return userCompetences; }
//    public void setUserCompetences(List<String> userCompetences) { this.userCompetences = userCompetences; }
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }
    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }
}


// SQLis :: admin: $2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei