package ee.rescue.tap.model;

public class Day {
    private String id; // 2019-09-09
    private String weekday; // Monday, Tuesday...
    private int weekdayValue; // 1-esmasp,2-teisip,3...
    private int hHolidayId; // 1-riigipüha,2-rahvuspüha,3-tähtpäev,4-lühendatud
    private String hTitle; // Uusaasta, Jaanipäev...
//    private String hKind; // riigipüha, rahvuspüha...
//    private Status workingStatus; // Enum

    public Day() { }

    public Day(String id, String weekDay, int weekdayValue, int hHolidayId, String hTitle) {
        this.id = id;
        this.weekday = weekDay;
        this.weekdayValue = weekdayValue;
        this.hHolidayId = hHolidayId;
        this.hTitle = hTitle;
//        this.workingStatus = workingStatus;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }
    public String getWeekday() { return weekday; }
    public void setWeekday(String weekday) { this.weekday = weekday; }
    public int getWeekdayValue() { return weekdayValue; }
    public void setWeekdayValue(int weekdayValue) { this.weekdayValue = weekdayValue; }
    public int gethHolidayId() { return hHolidayId; }
    public void sethHolidayId(int hHolidayId) { this.hHolidayId = hHolidayId; }
    public String gethTitle() { return hTitle; }
    public void sethTitle(String hTitle) { this.hTitle = hTitle; }
//    public String gethKind() { return hKind; }
//    public void sethKind(String hKind) { this.hKind = hKind; }
//    public Status getWorkingStatus() { return workingStatus; }
//    public void setWorkingStatus(Status workingStatus) { this.workingStatus = workingStatus; }
}
