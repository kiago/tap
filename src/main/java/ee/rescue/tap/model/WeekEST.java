package ee.rescue.tap.model;

public enum WeekEST {
    ESMASPÄEV,
    TEISIPÄEV,
    KOLMAPÄEV,
    NELJAPÄEV,
    REEDE,
    LAUPÄEV,
    PÜHAPÄEV;
}