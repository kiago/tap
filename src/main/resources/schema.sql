DROP TABLE IF EXISTS `userrole`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `usercompetence`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `brigade`;
DROP TABLE IF EXISTS `competence`;
DROP TABLE IF EXISTS `center`;
DROP TABLE IF EXISTS `workdays`;

CREATE TABLE IF NOT EXISTS `brigade` (
  `id` int(11) NOT NULL,
  `centerID` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `address` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `competence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brigadeID` int(11) NOT NULL DEFAULT 0,
  `squadNumber` int(1) DEFAULT 0,
  `firstName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `lastName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `idCode` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`idCode`),
  KEY `brigade_id` (`brigadeID`),
  CONSTRAINT `FK_brigade_id` FOREIGN KEY (`brigadeID`) REFERENCES `brigade` (`id`)
);

CREATE TABLE IF NOT EXISTS `usercompetence` (
  `user_id` int(11) DEFAULT NULL,
  `competence_id` int(11) DEFAULT NULL,
  KEY `Index 1` (`user_id`),
  KEY `Index 2` (`competence_id`),
  CONSTRAINT `FK_usercompetence_competence` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`),
  CONSTRAINT `FK_usercompetence_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE IF NOT EXISTS `userrole` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  KEY `Index 1` (`user_id`),
  KEY `Index 2` (`role_id`),
  CONSTRAINT `FK_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE IF NOT EXISTS `workdays` (
  `id` int(11) DEFAULT NULL,
  `m` int(11) DEFAULT NULL,
  `1` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `2` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `3` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `4` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `5` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `6` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `7` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `8` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `9` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `10` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `11` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `12` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `13` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `14` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `15` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `16` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `17` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `18` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `19` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `20` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `21` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `22` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `23` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `24` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `25` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `26` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `27` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `28` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `29` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `30` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `31` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
);
